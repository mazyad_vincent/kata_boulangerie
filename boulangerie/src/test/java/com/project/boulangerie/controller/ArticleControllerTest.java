package com.project.boulangerie.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.project.boulangerie.models.ArticleTypes;
import com.project.boulangerie.models.dto.ArticleDto;
import com.project.boulangerie.service.ArticleService;

@SpringBootTest
@AutoConfigureMockMvc
public class ArticleControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ArticleService articleService;

    private static ArticleDto validArticleDto;
    private static String API_URL_ARTICLES = "/api/articles";

    @BeforeAll
    public static void setUp() {
        validArticleDto = new ArticleDto((long) 1, "test", 2, (float) 50.0, ArticleTypes.MENU);
    }

    @Test
    public void testCreateArticleDto() throws Exception {
        when(articleService.post(any(Optional.class))).thenReturn(Optional.of(validArticleDto));

        mockMvc.perform(post(API_URL_ARTICLES)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Pain\", \"quantity\":10, \"price\":3.5, \"type\" : \"PAIN\"}"))
                .andExpect(status().isCreated())
                // .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("test"))
                .andExpect(jsonPath("$.price").value(50.0))
                .andExpect(jsonPath("$.quantity").value(2))
                .andExpect(jsonPath("$.type").value("MENU"));

        verify(articleService, times(1)).post(any(Optional.class));
    }

    @Test
    public void testUpdateArticleDto() throws Exception {
        when(articleService.update(eq(1L),
                any(Optional.class))).thenReturn(Optional.of(validArticleDto));

        mockMvc.perform(put(API_URL_ARTICLES + "/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Baguette\", \"price\":1.2}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("test"))
                .andExpect(jsonPath("$.price").value(50.0))
                .andExpect(jsonPath("$.type").value("MENU"))
                .andExpect(jsonPath("$.quantity").value(2));

        verify(articleService, times(1)).update(eq(1L),
                any(Optional.class));
    }

    @Test
    public void testDeleteProduit() throws Exception {
        Optional<Long> id = Optional.of(1L);
        when(articleService.delete(id)).thenReturn(id);

        mockMvc.perform(delete(API_URL_ARTICLES + "/1"))
                .andExpect(content().string("1"))
                .andExpect(status().isAccepted());

        verify(articleService, times(1)).delete(Optional.of(1L));
    }

    @Test
    public void testGetAllProduits() throws Exception {
        List<ArticleDto> articles = Arrays.asList(
                new ArticleDto(1L, "Pain", 15, (float) 1.0, ArticleTypes.PAIN),
                new ArticleDto(2L, "Menu", 1, (float) 20.0, ArticleTypes.MENU));
        when(articleService.selectAll()).thenReturn(Optional.of(articles));

        mockMvc.perform(get(API_URL_ARTICLES))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(2))
                .andExpect(jsonPath("$[0].name").value("Pain"))
                .andExpect(jsonPath("$[0].price").value(1.0))
                .andExpect(jsonPath("$[0].quantity").value(15))
                .andExpect(jsonPath("$[0].type").value("PAIN"))
                .andExpect(jsonPath("$[1].name").value("Menu"))
                .andExpect(jsonPath("$[1].price").value(20.0))
                .andExpect(jsonPath("$[1].quantity").value(1))
                .andExpect(jsonPath("$[1].type").value("MENU"));

        verify(articleService, times(1)).selectAll();
    }

}
