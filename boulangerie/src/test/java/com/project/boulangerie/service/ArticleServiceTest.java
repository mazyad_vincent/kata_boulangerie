package com.project.boulangerie.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.project.boulangerie.common.exception.RequestExceptions;
import com.project.boulangerie.mappers.ArticleMapper;
import com.project.boulangerie.models.ArticleTypes;
import com.project.boulangerie.models.dao.ArticleDao;
import com.project.boulangerie.models.dto.ArticleDto;
import com.project.boulangerie.repository.ArticleRepository;

@ExtendWith(MockitoExtension.class)
public class ArticleServiceTest {

    @Mock
    private ArticleRepository articleRepository;

    @Mock
    private ArticleMapper articleMapper;

    @InjectMocks
    private ArticleService articleService;

    private static List<ArticleDao> articles;
    private static ArticleDto validArticleDto;

    @BeforeAll
    public static void setUp() {
        ArrayList<ArticleDao> articlesList = new ArrayList<>();
        for (int i = 0; i < 5; ++i) {
            ArticleDao dao = new ArticleDao(Long.valueOf(i), "article " + String.valueOf(i), ArticleTypes.PAIN, i + 5,
                    (float) 2 * i);
            articlesList.add(dao);
        }
        articles = articlesList;

        validArticleDto = new ArticleDto((long) 1, "test", 2, (float) 50.0, ArticleTypes.MENU);
    }

    @Test
    public void testSelectAllSuccess() {
        when(articleRepository.findAll()).thenReturn(articles);
        when(articleMapper.convertFromDaoToDto(any())).thenReturn(validArticleDto);
        Optional<List<ArticleDto>> result = articleService.selectAll();
        assertTrue(result.isPresent());
        assertTrue(result.get().size() == articles.size(), "La liste devrait être de 5");
        assertTrue(result.get().get(0).getId() == 1, "Mauvaise conversion avec le mapper");
        assertSame(result.get().get(0), result.get().get(1), "les objets sont les mêmes");
    }

    @Test
    public void testPostSuccess() {
        ArticleDao dao = new ArticleDao((long) 2, "testpost", ArticleTypes.MENU, 20, (float) 10.0);
        when(articleRepository.save(any(ArticleDao.class))).thenReturn(dao);
        when(articleMapper.convertFromDtoToDao(any())).thenReturn(dao);
        Optional<ArticleDto> result = articleService.post(Optional.of(validArticleDto));

        // Assert
        assertTrue(result.isPresent());
        assertEquals(validArticleDto, result.get());
    }

    @Test
    public void testPostWithEmptyDto_ShouldThrowException400() {
        Optional<ArticleDto> emptyDto = Optional.empty();
        assertThrows(RequestExceptions.Exception400.class, () -> articleService.post(emptyDto));
    }

    @Test
    public void testPost_WithInvalidDto_ShouldThrowException400() {
        assertThrows(RequestExceptions.Exception400.class, () -> {
            articleService.post(Optional.of(new ArticleDto()));
        });
    }

    @Test
    public void testPost_WithException_ShouldThrowException500() {

        when(articleRepository.save(any(ArticleDao.class))).thenThrow(new RuntimeException("Database error"));

        assertThrows(RequestExceptions.Exception500.class, () -> {
            articleService.post(Optional.of(validArticleDto));
        });
    }

    @Test
    public void testDeleteValidDto() {
        long id = 1;
        when(articleRepository.findById(id)).thenReturn(Optional.of(new ArticleDao()));
        articleService.delete(Optional.of(id));
        verify(articleRepository, times(1)).deleteById(id);
    }

    @Test
    public void testDeleteNotFoundDto_ShouldThrow404() {
        when(articleRepository.findById(anyLong())).thenReturn(Optional.ofNullable(null));
        assertThrows(RequestExceptions.Exception404.class, () -> articleService.delete(Optional.of((long) 1)));
        verify(articleRepository, never()).deleteById((long) 1);
    }

    @Test
    public void testDeleteMissingId_ShouldThrow400() {
        assertThrows(RequestExceptions.Exception400.class, () -> articleService.delete(Optional.empty()));
    }

    @Test
    public void testUpdateSuccess() {
        long id = 1;
        ArticleDao dao = new ArticleDao((long) 1, "test", ArticleTypes.GATEAU, 50, (float) 10.0);
        when(articleRepository.findById(id)).thenReturn(Optional.of(dao));
        articleService.update(id, Optional.of(validArticleDto));

        assertEquals(validArticleDto.getName(), dao.getName());
        assertEquals(validArticleDto.getQuantity(), dao.getQuantity());
        assertEquals(validArticleDto.getPrice(), dao.getPrice());
        assertEquals(validArticleDto.getType(), dao.getType());

        verify(articleRepository, times(1)).save(dao);
    }

    @Test
    public void testUpdateInvalidParameters_ShouldTrow400() {
        ArticleDto invalidDto = new ArticleDto();
        assertThrows(RequestExceptions.Exception400.class, () -> articleService.update(null, Optional.of(validArticleDto)));
        assertThrows(RequestExceptions.Exception400.class, () -> articleService.update((long) 1, Optional.of(invalidDto)));  
    }

    @Test
    public void testUpdateNonExistingArticle_ShouldThrow404() {
        when(articleRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(RequestExceptions.Exception404.class, () -> articleService.update((long) 1, Optional.of(validArticleDto)));
        verify(articleRepository, never()).save(any());
    }


    @Test
    public void testValidDtoIsValid() {
        assertTrue(articleService.validDto(validArticleDto));
    }

    @Test
    public void testValidDtoIsInvalid() {
        ArticleDto badQuantity = new ArticleDto((long) 1, "test", -1, (float) 50.0, ArticleTypes.MENU);
        ArticleDto badPrice = new ArticleDto((long) 1, "test", -1, (float) -1, ArticleTypes.MENU);
        ArticleDto missingName = new ArticleDto((long) 1, null, -1, (float) -1, ArticleTypes.MENU);
        ArticleDto missingType = new ArticleDto((long) 1, null, -1, (float) -1, null);
        
        assertFalse(articleService.validDto(badQuantity));
        assertFalse(articleService.validDto(badPrice));
        assertFalse(articleService.validDto(missingName));
        assertFalse(articleService.validDto(missingType));
    }
}
