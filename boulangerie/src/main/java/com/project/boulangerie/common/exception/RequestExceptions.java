package com.project.boulangerie.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Class parent of all Request Exceptions
 */

public class RequestExceptions {

	private static final String internal_error = "Erreur interne du serveur";
	private static final String notfound_error = "L'élement recherché n'existe pas";
	private static final String access_error = "Vous n'avez pas les droits d'accès sur cet élement";
	private static final String bad_request_error = "Mauvaise Requête utilisateur";
	private static final String unauthorized_error = "Mail ou mot de passe incorrect";

	@ResponseStatus(HttpStatus.NOT_FOUND)
	public static class Exception404 extends RuntimeException {

		public Exception404(final String message) {
			super(message);
		}

		public Exception404() {
			super(notfound_error);
		}
	}

	@ResponseStatus(HttpStatus.FORBIDDEN)
	public static class Exception403 extends RuntimeException {

		public Exception403(final String message) {
			super(message);
		}

		public Exception403() {
			super(access_error);
		}
	}

	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public static class Exception401 extends RuntimeException {

		public Exception401(final String message) {
			super(message);
		}

		public Exception401() {
			super(unauthorized_error);
		}
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public static class Exception400 extends RuntimeException {
		public Exception400(final String message) {
			super(message);
		}

		public Exception400() {
			super(bad_request_error);
		}
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public static class Exception500 extends RuntimeException {

		public Exception500(final String message) {
			super(message);
		}

		public Exception500() {
			super(internal_error);
		}
	}

}
