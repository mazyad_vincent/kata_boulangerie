package com.project.boulangerie.common.exception;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler {

	@ExceptionHandler(value = { RequestExceptions.Exception400.class })
	public ResponseEntity<Object> handlerApiRequestException(final RequestExceptions.Exception400 e) {
		final var httpStatus = HttpStatus.BAD_REQUEST;
		final var exception = new ApiExceptionModel(e.getMessage(), ZonedDateTime.now(ZoneId.of("Z")),
				httpStatus.value());
		return new ResponseEntity<>(exception, httpStatus);
	}

	@ExceptionHandler(value = { RequestExceptions.Exception404.class })
	public ResponseEntity<Object> handlerApiRequestException(final RequestExceptions.Exception404 e) {
		final var httpStatus = HttpStatus.NOT_FOUND;
		final var exception = new ApiExceptionModel(e.getMessage(), ZonedDateTime.now(ZoneId.of("Z")),
				httpStatus.value());
		return new ResponseEntity<>(exception, httpStatus);
	}

	@ExceptionHandler(value = { RequestExceptions.Exception403.class })
	public ResponseEntity<Object> handlerApiRequestException(final RequestExceptions.Exception403 e) {
		final var httpStatus = HttpStatus.FORBIDDEN;
		final var exception = new ApiExceptionModel(e.getMessage(), ZonedDateTime.now(ZoneId.of("Z")),
				httpStatus.value());
		return new ResponseEntity<>(exception, httpStatus);
	}

	@ExceptionHandler(value = { RequestExceptions.Exception401.class })
	public ResponseEntity<Object> handlerApiRequestException(final RequestExceptions.Exception401 e) {
		final var httpStatus = HttpStatus.UNAUTHORIZED;
		final var exception = new ApiExceptionModel(e.getMessage(), ZonedDateTime.now(ZoneId.of("Z")),
				httpStatus.value());
		return new ResponseEntity<>(exception, httpStatus);
	}

	@ExceptionHandler(value = { RequestExceptions.Exception500.class })
	public ResponseEntity<Object> handlerApiRequestException(final RequestExceptions.Exception500 e) {
		final var httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		final var exception = new ApiExceptionModel(e.getMessage(), ZonedDateTime.now(ZoneId.of("Z")),
				httpStatus.value());
		return new ResponseEntity<>(exception, httpStatus);
	}
}
