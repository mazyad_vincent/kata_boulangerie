package com.project.boulangerie.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.boulangerie.common.exception.RequestExceptions;
import com.project.boulangerie.models.dto.UserDto;
import com.project.boulangerie.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequestMapping("/api/users")
@RestController
public class UserController {

    @Autowired
    UserService userService;

    /**
     * @author vince
     * @return la liste des utilisateurs enregistrées en bdd
     */

    @GetMapping(produces = "application/json", value = "/all")
    public ResponseEntity<List<Object>> getAll() {
        log.info("récupération des offres");
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    /**
     * enregistre un utilisateur
     * 
     * @return l'utilisateur enregistré en bdd
     */

    @PostMapping(produces = "application/json")
    public ResponseEntity<String> registerUser(@AuthenticationPrincipal JwtAuthenticationToken authentication) {
        // Obtenir les informations de l'utilisateur à partir du token
        Jwt jwt = authentication.getToken();
        String email = jwt.getClaim("email");

        // Vérifier si l'utilisateur existe déjà dans la base de données
        try {
            userService.findByEmail(Optional.ofNullable(email));
            return ResponseEntity.status(HttpStatus.OK).body("Utilisateur déja crée");
        } catch (RequestExceptions.Exception404 e) {
            userService.registerUserViaOauth(Optional.ofNullable(email));
            return ResponseEntity.status(HttpStatus.CREATED).body("L'utilisateur a été crée");
        }
    }

    @GetMapping("/user-info")
    public UserDto getUserInfo(@AuthenticationPrincipal JwtAuthenticationToken authentication) {
        // Obtenir les informations de l'utilisateur à partir du token
        Jwt jwt = authentication.getToken();
        String email = jwt.getClaim("email");

        // Récupérer les informations de l'utilisateur depuis la base de données
        return userService.findByEmail(Optional.ofNullable(email));
    }

    /**
     * met à jour les donnés d'un utilisateur
     * 
     * @param les données modifiées de l'utilisateur
     * @return l'entité modifiée
     */

    /*
     * @ApiOperation(value = "met à jour les données d'un utilisateur", produces =
     * "application/json")
     */
    @PutMapping(produces = "application/json", value = "update")
    public ResponseEntity<Object> update(/* @RequestBody ProductDto offer */) {
        log.info("modification d'une offre");
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

}
