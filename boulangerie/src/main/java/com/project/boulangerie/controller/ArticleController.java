package com.project.boulangerie.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.boulangerie.common.exception.ApiExceptionModel;
import com.project.boulangerie.common.exception.RequestExceptions;
import com.project.boulangerie.models.dto.ArticleDto;
import com.project.boulangerie.service.ArticleService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequestMapping("/api/articles")
@RestController
public class ArticleController {

	@Autowired
	ArticleService articleService; // le service associé au controller article

	/**
	 * @author vince
	 * @return la liste des utilisateurs enregistrées en bdd
	 */

	@Operation(summary = "Récupère tous les articles")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Ok", content = {
			@Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = ArticleDto.class))) }),
			@ApiResponse(responseCode = "500", description = "Erreur interne", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ApiExceptionModel.class)) }) })

	@GetMapping(produces = "application/json")
	public ResponseEntity<List<ArticleDto>> getAll() {
		log.info("récupération des offres");
		final Optional<List<ArticleDto>> retValue = articleService.selectAll();
		if (retValue.isEmpty()) {
			throw new RequestExceptions.Exception500();
		}
		return ResponseEntity.status(HttpStatus.OK).body(retValue.get());
	}

	/**
	 * enregistre un utilisateur
	 *
	 * @return l'utilisateur enregistré en bdd
	 */
	@Operation(summary = "ajoute un article")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Ok", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ArticleDto.class)) }),
			@ApiResponse(responseCode = "400", description = "Paramètres invalides", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ApiExceptionModel.class)) }),
			@ApiResponse(responseCode = "500", description = "Erreur interne", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ApiExceptionModel.class)) }) })

	@PostMapping(produces = "application/json", consumes = "application/json")
	public ResponseEntity<ArticleDto> postArticle(@RequestBody final ArticleDto dto) {
		log.info("ajout d'une offre");
		return ResponseEntity.status(HttpStatus.CREATED).body(articleService.post(Optional.ofNullable(dto)).get());
	}

	/**
	 * met à jour les données d'un article
	 *
	 * @param les données modifiées de l'article
	 * @return l'entité modifiée
	 */

	/**
	 * enregistre un utilisateur
	 *
	 * @return l'utilisateur enregistré en bdd
	 */
	@Operation(summary = "modifie un article")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Ok", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ArticleDto.class)) }),
			@ApiResponse(responseCode = "400", description = "Paramètres invalides", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ApiExceptionModel.class)) }),
			@ApiResponse(responseCode = "500", description = "Erreur interne", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ApiExceptionModel.class)) }) })

	@PutMapping(value = "/{id}", produces = "application/json", consumes = "application/json")
	public ResponseEntity<ArticleDto> update(@PathVariable Long id, @RequestBody ArticleDto article) {
		log.info("modification d'une offre");
		return ResponseEntity.status(HttpStatus.OK).body(articleService.update(id, Optional.ofNullable(article)).get());
	}

	/**
	 * supprime un article selon son id
	 *
	 * @param id l'id de l'article à supprimer
	 * @return l'id de l'entité supprimée
	 */

	@Operation(summary = "supprime un article selon son id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "202", description = "Ok", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Long.class)) }),
			@ApiResponse(responseCode = "400", description = "Paramètres invalides", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ApiExceptionModel.class)) }),
			@ApiResponse(responseCode = "404", description = "article non trouvé", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ApiExceptionModel.class)) }),
			@ApiResponse(responseCode = "500", description = "Erreur interne", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ApiExceptionModel.class)) }) })

	@DeleteMapping("/{id}")
	public ResponseEntity<Long> delete(@PathVariable final Long id) {
		log.info("suppression d'un article");
		articleService.delete(Optional.ofNullable(id));
		return ResponseEntity.accepted().body(id);
	}

}
