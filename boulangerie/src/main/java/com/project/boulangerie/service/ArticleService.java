package com.project.boulangerie.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.project.boulangerie.common.exception.RequestExceptions;
import com.project.boulangerie.mappers.ArticleMapper;
import com.project.boulangerie.mappers.BasicMapper;
import com.project.boulangerie.models.dao.ArticleDao;
import com.project.boulangerie.models.dto.ArticleDto;
import com.project.boulangerie.repository.ArticleRepository;

@Service
public class ArticleService extends BasicService<ArticleDto, ArticleDao> {

	@Autowired
	ArticleRepository articleRepository;
	@Autowired
	ArticleMapper articleMapper;

	@Override
	public Optional<ArticleDto> post(Optional<ArticleDto> dto) {
		if (!dto.isEmpty() && dto.get().getQuantity() == null)
			dto.get().setQuantity(0); // on initialise la quantité à 0 si le stock n'a pas été défini dans la requête
		return super.post(dto);
	}

	/**
	 * modifie l'entité avec les valeurs présentes dans article
	 * 
	 * @param dto
	 * @return la nouvelle entité modifiée
	 */
	public Optional<ArticleDto> update(Long id, final Optional<ArticleDto> dto) {
		if (dto.isEmpty() || id == null || !validDto(dto.get())) {
			throw new RequestExceptions.Exception400("il manque les informations de l'entité à modifier");
		}
		var articleDto = dto.get();
		Optional<ArticleDto> retValue = articleRepository.findById(id).map(article -> {
			article.setName(articleDto.getName());
			article.setPrice(articleDto.getPrice());
			article.setQuantity(articleDto.getQuantity());
			article.setType(articleDto.getType());
			articleRepository.save(article);
			return Optional.ofNullable(articleMapper.convertFromDaoToDto(article));
		}).orElseThrow(() -> new RequestExceptions.Exception404("article non trouvé"));
		return retValue;
	}

	@Override
	public boolean validDto(final ArticleDto dto) {
		return dto != null && validString(dto.getName()) && dto.getQuantity() != null && dto.getQuantity() >= 0 &&
				dto.getPrice() != null && dto.getPrice() >= 0.0f && dto.getType() != null;
	}

	@Override
	protected BasicMapper<ArticleDto, ArticleDao> getMapper() {
		return articleMapper;
	}

	@Override
	protected JpaRepository<ArticleDao, Long> getRepository() {
		return articleRepository;
	}
}
