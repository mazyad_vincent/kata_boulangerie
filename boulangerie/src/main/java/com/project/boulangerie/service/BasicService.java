package com.project.boulangerie.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.boulangerie.common.exception.RequestExceptions;
import com.project.boulangerie.mappers.BasicMapper;
import com.project.boulangerie.models.dao.BasicDao;
import com.project.boulangerie.models.dto.BasicDto;

import lombok.extern.slf4j.Slf4j;

/**
 * super class utilisée pour les Services ...)
 *
 * @param <Z> le Modèle Dto associé au service
 * @param <T> le Modèle Dao associé au service
 */
@Slf4j
public abstract class BasicService<Z extends BasicDto, T extends BasicDao> {
	/**
	 * récupère toutes les entités
	 *
	 * @return la liste d'entité que l'on souhaite récupérer sous forme de DTO
	 */
	public Optional<List<Z>> selectAll() {
		try {
			log.debug("selection de toutes les entitées de " + getClass().getName());
			final List<T> repositoryList = getRepository().findAll();
			return Optional.ofNullable(repositoryList.stream().map(element -> getMapper().convertFromDaoToDto(element))
					.collect(Collectors.toList()));

		} catch (final Exception e) {
			log.error("erreur au niveau de la selection de toutes les entitées de " + getClass().getName()
					+ " exception levée : " + e.getMessage());
			throw new RequestExceptions.Exception500();
		}
	}

	/**
	 * poste une entité
	 *
	 * @param dto le body que l'utilisateur souhaite ajouter
	 * @return l'élément ajouté par l'utilisateur
	 */
	public Optional<Z> post(final Optional<Z> dto) {
		if (dto.isEmpty() || !validDto(dto.get())) {
			throw new RequestExceptions.Exception400("Il manque des arguments");
		}
		try {
			getRepository().save(getMapper().convertFromDtoToDao(dto.get()));
			return dto;
		} catch (final Exception e) {
			log.error("erreur ajout de : " + getClass().getName() + " exception levée : " + e.getMessage());
			throw new RequestExceptions.Exception500();
		}
	}

	/**
	 * supprime l'entité qui a comme id <id>
	 *
	 * @param Optional<Long> id
	 * @return l'entité supprimée
	 */
	public Optional<Long> delete(final Optional<Long> id) {
		if (id == null || id.isEmpty()) {
			throw new RequestExceptions.Exception400("il manque l'id de l'entité à supprimer");
		}
		Optional<T> dataStored = getRepository().findById(id.get());
		if (dataStored.isEmpty()) {
			log.error("suppression d'une entité inexistante : l'id est : " + id.get() + " classe : "
					+ getClass().getName());
			throw new RequestExceptions.Exception404();
		}
		getRepository().deleteById(id.get());
		return id;
	}

	/**
	 * vérifie qu'une chaine de caractère n'est pas vide
	 *
	 * @param str la chaine de caractère à vérifier
	 * @return true si la chaine n'est pas vide
	 */
	protected boolean validString(final String str) {
		return str != null && str.length() >= 1;
	}

	/**
	 * récupére le répertoire JPA associé
	 *
	 * @return le répertoire JPA
	 */
	protected abstract JpaRepository<T, Long> getRepository();

	/**
	 * récupère le mapper
	 *
	 * @return le mapper
	 */
	protected abstract BasicMapper<Z, T> getMapper();

	/**
	 * Vérifie que le dto est valide (vérifications des paramétresà
	 *
	 * @param dto
	 * @return true si le dto est valide, false sinon
	 */
	protected abstract boolean validDto(final Z dto);

}
