package com.project.boulangerie.service;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.boulangerie.common.exception.RequestExceptions;
import com.project.boulangerie.mappers.BasicMapper;
import com.project.boulangerie.mappers.UserMapper;
import com.project.boulangerie.models.RolesTypes;
import com.project.boulangerie.models.dao.RoleDao;
import com.project.boulangerie.models.dao.UserDao;
import com.project.boulangerie.models.dto.UserDto;
import com.project.boulangerie.repository.UserRepository;

@Service
public class UserService extends BasicService<UserDto, UserDao> {
	@Autowired
	UserRepository userRepository;
	@Autowired
	UserMapper userMapper;

	private static final String GOOGLE_PROVIDER_NAME = "google";

	public UserDto findByEmail(Optional<String> username) {
		if (username.isEmpty() || username.get() == null) {
			throw new RequestExceptions.Exception500();
		}

		Optional<UserDao> user = userRepository.findByEmail(username.get());
		if (user.isEmpty() || user.get() == null) {
			throw new RequestExceptions.Exception404();
		}
		return userMapper.convertFromDaoToDto(user.get());
	}

	/**
	 * enregistre l'utilisateur via oAuth2
	 * 
	 * @param email l'email de l'utilisateur
	 * @return true si l'utilisateur a été enregistré
	 */
	public void registerUserViaOauth(Optional<String> email) {
		if (email.isEmpty() || !validString(email.get())) {
			throw new RequestExceptions.Exception400();
		}
		final Set<RoleDao> roles = Set.of(new RoleDao(null, RolesTypes.USER));
		UserDao userToSave = new UserDao(null, "email", null, false, GOOGLE_PROVIDER_NAME, roles, null);
		userRepository.save(userToSave);
	}

	@Override
	protected JpaRepository<UserDao, Long> getRepository() {
		return userRepository;
	}

	@Override
	protected BasicMapper<UserDto, UserDao> getMapper() {
		return userMapper;
	}

	@Override
	protected boolean validDto(UserDto dto) {
		return true; // FIXME
	}
}
