package com.project.boulangerie.models.dto;

import com.project.boulangerie.models.ArticleTypes;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ArticleDto extends BasicDto {
	/* @ApiModelProperty(notes = "base de donnée auto generée id") */
	/* @ApiModelProperty(notes = "le nom de produit (ex : tee shirt)") */
	private String name;
	/* @ApiModelProperty(notes = "la quantité de produit à acheter") */
	private Integer quantity;
	/* @ApiModelProperty(notes = "le prix") */
	private Float price;
	@Enumerated(EnumType.STRING)
	private ArticleTypes type;

	public ArticleDto(Long id, String name, Integer quantity, Float price, ArticleTypes type) {
		this.id = id;
		this.name = name;
		this.quantity = quantity;
		this.price = price;
		this.type = type;
	}

}
