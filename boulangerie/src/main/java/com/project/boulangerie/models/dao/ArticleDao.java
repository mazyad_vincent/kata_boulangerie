package com.project.boulangerie.models.dao;

import com.project.boulangerie.models.ArticleTypes;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "article")
public class ArticleDao implements BasicDao {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "name", nullable = false)
	private String name;
	@Column(name = "type", nullable = false)
	@Enumerated(EnumType.STRING)
	private ArticleTypes type;
	@Column(name = "quantity", nullable = false, columnDefinition = "INT CHECK (quantity >= 0)")
	private Integer quantity;
	@Column(name = "price", nullable = false, columnDefinition = "FLOAT CHECK (price >= 0)")
	private Float price;
}
