package com.project.boulangerie.models;

public enum ArticleTypes {
	VIENNOISERIE, GATEAU, PAIN, MENU
}
