package com.project.boulangerie.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.boulangerie.models.dao.UserDao;

public interface UserRepository extends JpaRepository<UserDao, Long> {
    Optional<UserDao> findByEmail(String email);
}