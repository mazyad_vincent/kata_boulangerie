package com.project.boulangerie.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.boulangerie.models.dao.ArticleDao;

public interface ArticleRepository extends JpaRepository<ArticleDao, Long> {

}
