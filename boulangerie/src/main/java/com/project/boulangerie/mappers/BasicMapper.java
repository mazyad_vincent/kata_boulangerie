package com.project.boulangerie.mappers;

import com.project.boulangerie.models.dao.BasicDao;
import com.project.boulangerie.models.dto.BasicDto;

/**
 * Mapper pour convertir un DTO en DAO et inversement Utilisation de la lib
 * MapStruct
 */
public interface BasicMapper<Z extends BasicDto, T extends BasicDao> {

	/**
	 * convertis un Dao en Dto
	 *
	 * @param dao
	 * @return le dto associé
	 */
	public Z convertFromDaoToDto(T dao);

	/**
	 * convertis un Dto en Dao
	 *
	 * @param dto
	 * @return le Dao associé
	 */
	public T convertFromDtoToDao(Z dto);

}
