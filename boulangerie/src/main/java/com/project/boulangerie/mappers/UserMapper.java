package com.project.boulangerie.mappers;

import org.mapstruct.Mapper;

import com.project.boulangerie.models.dao.UserDao;
import com.project.boulangerie.models.dto.UserDto;

@Mapper(componentModel = "spring")
public interface UserMapper extends BasicMapper<UserDto, UserDao> {

    @Override
    UserDto convertFromDaoToDto(UserDao dao);

    @Override
    UserDao convertFromDtoToDao(UserDto dto);
}
