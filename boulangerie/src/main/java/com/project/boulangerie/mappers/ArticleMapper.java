package com.project.boulangerie.mappers;

import org.mapstruct.Mapper;

import com.project.boulangerie.models.dao.ArticleDao;
import com.project.boulangerie.models.dto.ArticleDto;

/**
 * mapper pour les articles
 */

@Mapper(componentModel = "spring")
public interface ArticleMapper extends BasicMapper<ArticleDto, ArticleDao> {
	@Override
	ArticleDto convertFromDaoToDto(ArticleDao dao);

	@Override
	ArticleDao convertFromDtoToDao(ArticleDto dto);
}
