import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlePresentationComponent } from './article-presentation.component';

describe('ArticlePresentationComponent', () => {
  let component: ArticlePresentationComponent;
  let fixture: ComponentFixture<ArticlePresentationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ArticlePresentationComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ArticlePresentationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
