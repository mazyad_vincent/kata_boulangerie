import { Component, Input } from '@angular/core';
import {MatCardModule} from '@angular/material/card';

@Component({
  selector: 'app-article-presentation',
  standalone: true,
  imports: [MatCardModule],
  templateUrl: './article-presentation.component.html',
  styleUrl: './article-presentation.component.scss'
})
export class ArticlePresentationComponent {

  @Input() // image source of the image
  image_src: String = '';

  @Input()
  description: String = '';

  @Input()
  title: String = '';

  constructor() { }

  ngOnInit() {
  }
}
