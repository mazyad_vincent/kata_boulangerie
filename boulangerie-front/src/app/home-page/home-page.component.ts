import { CommonModule, isPlatformBrowser } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit, PLATFORM_ID } from '@angular/core';
import { ArticlePresentationComponent } from './article-presentation/article-presentation.component';
import { CarouselModule } from 'primeng/carousel';

@Component({
  selector: 'app-home-page',
  standalone: true,
  imports: [ArticlePresentationComponent, CommonModule, CarouselModule],
  templateUrl: './home-page.component.html',
  styleUrl: './home-page.component.scss'
})
export class HomePageComponent implements OnDestroy, OnInit {
  // pour l'animation avec l'intersection observer
  readonly intersectionRatio = .1;
  readonly config = {
    root: null,
    rootMargin: '0px',
    threshold: this.intersectionRatio
  }
  observer: IntersectionObserver | undefined;

  // image background 
  readonly backgroundImages = [{
    image: "/assets/homepage/fondImageViennoiseries.jpg",
    alt: "Viennoiseries"
  }, {
    image: "/assets/homepage/fondImageGateaux.jpg",
    alt: "Gateaux"
  }, {
    image: "/assets/homepage/fondImageSandwichs.jpg",
    alt: "Sandwichs"
  }]

  // index courant de l'image que l'on souhaite afficher à l'écran
  currentImageIndex: number = 0;

  // chemins images
  readonly PATH_CAKE = "/assets/homepage/cake.jpg";
  readonly PATH_BREAD = "/assets/homepage/pain.jpg";
  readonly PATH_SANDWICH = "/assets/homepage/sandiwch.jpg";
  readonly PATH_VIENNOISERIES = "/assets/homepage/viennoiseries.jpg";

  //descriptif de l'image
  readonly DESCRIPTIF_CAKE = "Nos gâteaux peuvent répondre à vos besoins individuels, possibilité dedécoration & ingéniosité sans pareils"
  readonly DESCRIPTIF_BREAD = "Nos Pains peuvent répondre à vos besoins individuels, possibilité dedécoration & ingéniosité sans pareils"
  readonly DESCRIPTIF_SANDWICH = "Nos Sandwichs peuvent répondre à vos besoins individuels, possibilité dedécoration & ingéniosité sans pareils"
  readonly DESCRIPTIF_VIENNOISERIES = "Nos Viennoiseries peuvent répondre à vos besoins individuels, possibilité dedécoration & ingéniosité sans pareils"


  constructor(@Inject(PLATFORM_ID) private platformId: Object) {

  }

  ngOnInit() {
    /* utilise l'intersection observer pour ajouter une animation quand l'utilisateur arrive sur 
                              l'element à animer (specifier par la classe reveal)
                              on est sur une application executée en ssr (server side rendering)
                              le intersectionObserver n'est défini que sur le navigateur, il faut vérifier que le code s'execute coté client */
    if (isPlatformBrowser(this.platformId)) {
      this.observer = new IntersectionObserver(this.handleAnimation, this.config);
      document.querySelectorAll(".reveal").forEach((htmlElement) => this.observer!.observe(htmlElement));
    }
  }

  /**
   * callback lancé une fois que notre element est visible grâce à l'intersection observer
   */
  handleAnimation = (entries: Array<IntersectionObserverEntry>, observer: IntersectionObserver) => {
    entries.forEach((entry) => {
      if (entry.intersectionRatio > this.intersectionRatio) {
        entry.target.classList.add('reveal-visible');
        observer.unobserve(entry.target);
      }
    })
  }

  getNextImage = (): void => {
    this.currentImageIndex = (this.currentImageIndex + 1) % this.backgroundImages.length;
  }

  getPreviousImage = (): void => {
    this.currentImageIndex = this.currentImageIndex === 0 ? this.backgroundImages.length - 1 : this.currentImageIndex - 1;
  }

  ngOnDestroy() {
    if (this.observer) {
      this.observer.disconnect();
    }
  }
}
