import { CommonModule } from '@angular/common';
import { Component, HostListener } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ScrollDirection } from './scrollDirection';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss'
})
export class NavbarComponent {
  private current_scroll_position = 0; // position courante du scroll dans la fenetre

  ScrollDirection = ScrollDirection;
  public scrollDirection = ScrollDirection.MaxTop; // direction

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  /**
   * modifie la proprieté du header selon si on scroll vers le bas ou en haut
   * @param event 
   */

  @HostListener('window:scroll', ['$event']) // scroll fenetre
  onScroll(event: Event) {
    const pos = document.documentElement.scrollTop;
    if (pos == 0)
      this.scrollDirection = ScrollDirection.MaxTop;
    else if (pos > this.current_scroll_position)
      this.scrollDirection = ScrollDirection.Bottom;
    else
      this.scrollDirection = ScrollDirection.Top;
    this.current_scroll_position = pos;
  }


  onClickOutside(event: Event) { // ferme le menu de navigation quand l'utilisateur clique en dehors
    if (!(<Element>event.target).className.match(new RegExp('bar.|toggle-button'))) { // verifie qu'on clique pas sur les 3 petites barres pour ouvrir le menu
      this.OpenCloseNavigationHeader("remove"); // ferme le menu de navigation
    }
  }

  /**
  * ouvre ou ferme le menu de navigation
  * @param action le type d'action que l'on souhaite réaliser (toggle pour activer/desactiver, remove pour desactiver)
  */
  OpenCloseNavigationHeader(action: String) {
    const navbarLinks = document.getElementsByClassName('navbar-links')[0];
    if (action === "toggle") { /* on active ou desactive (toggle) le menu de navigation */
      document.querySelectorAll("span").forEach((item) => item.classList.toggle('active'))
      navbarLinks.classList.toggle('active');
    }

    else if (action === "remove") { /* on veut enlever le menu de navigation du mode mobile */
      document.querySelectorAll("span").forEach((item) => item.classList.remove('active'))
      navbarLinks.classList.remove('active');
    }
  }
}
