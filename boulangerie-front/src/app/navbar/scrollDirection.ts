export enum ScrollDirection {
    MaxTop, // utilisateur a scrollé tout en haut
    MaxBottom, // l'utilisateur a  scrollé tout en bas
    Bottom,
    Top,
}